package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class RepairOrder implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    boolean isPaid;
    private LocalDate dateAdded;

    // czytamy : wiele samochodów do jednego właściciela
    @ManyToOne
    private Car car;

}
