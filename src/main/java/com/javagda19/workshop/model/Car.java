package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Car implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String registrationNumber;
    private String manufasturer;
    private String model;
    private int productionYear;

    public Car(String registrationNumber, String manufasturer, String model, int productionYear) {
        this.registrationNumber = registrationNumber;
        this.manufasturer = manufasturer;
        this.model = model;
        this.productionYear = productionYear;
    }

    // czytamy : wiele samochodów do jednego właściciela
    @ManyToOne
    private Owner owner;

    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
    private Set<RepairOrder> repairOrders;

}
