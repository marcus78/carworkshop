package com.javagda19.workshop.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
 // Data Acces Object DAO
@Entity
@Getter
@ToString
@NoArgsConstructor
public class Owner implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Owner(String name, String lastName, String PESEL) {
        this.name = name;
        this.lastName = lastName;
        this.PESEL = PESEL;
    }

    private String name;
    private String lastName;
    private String PESEL;

    // jeśli będę chciał dowiedzieć się
    // jakie samochody ma dany właściciel
    // to powinienem zrobić relację
   @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    //@OneToMany(mappedBy = "owner")
    private List<Car> cars;

}
