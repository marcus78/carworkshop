package com.javagda19.workshop;

import com.javagda19.workshop.logic.EntityDao;
import com.javagda19.workshop.logic.HibernateUtil;
import com.javagda19.workshop.model.Car;
import com.javagda19.workshop.model.Owner;
import javafx.scene.transform.Scale;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        EntityDao entityDao = new EntityDao();
//        entityDao.save(new Owner());

        EntityDao entityDao = new EntityDao();
        Scanner scanner = new Scanner(System.in);

        boolean working = true;

        do {
            String linia = scanner.nextLine();
            if (linia.equalsIgnoreCase("quit")) {
                working = false;
            } else {
                String[] slowa = linia.split(" ");
                String komenda = slowa[0];
                if (komenda.equalsIgnoreCase("dodaj") && slowa[1].equalsIgnoreCase("owner")) {
                    //tutaj logika dodawania ownera
                    addOwner(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("dodaj") && slowa[1].equalsIgnoreCase("car")) {
                    //tutaj logika dodawania car
                    addCar(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("list") && slowa[1].equalsIgnoreCase("car")) {
                    //tutaj logika dodawania car
                    List<Car> cars = entityDao.getListOfAll(Car.class);
                    cars.forEach(System.out::println);
                    //addCar(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("list") && slowa[1].equalsIgnoreCase("owner")) {
                    //tutaj logika dodawania car
                    List<Owner> owners = entityDao.getListOfAll(Owner.class);
                    owners.forEach(System.out::println);
                    //addCar(entityDao, slowa);
                }
                else if (komenda.equalsIgnoreCase("powiaz")){
                    Long ownerId = Long.parseLong(slowa[1]);
                    Long carId = Long.parseLong(slowa[2]);

                    Optional<Owner> ownerOptional = entityDao.getById(Owner.class, ownerId);
                    Optional<Car> carOptional = entityDao.getById(Car.class, carId);

                    if(ownerOptional.isPresent() && carOptional.isPresent()){

                        Owner owner = ownerOptional.get();
                        Car car = carOptional.get();

                        owner.getCars().add(car);
                        car.setOwner(owner);

                        entityDao.save(car);
                        entityDao.save(owner);
                    }else{

                        System.err.println("Błąd, nie mogę znaleźć obiektu.");

                    }
                }
            }
        } while (working);

        HibernateUtil.getSessionFactory().close();
    }

    private static void addCar(EntityDao entityDao, String[] slowa) {
        String registrationNumber = slowa[2]; // mozna stworzyc alias tutaj
        Car car = new Car(registrationNumber, slowa[3], slowa[4], Integer.parseInt(slowa[5]));
        entityDao.save(car);
    }

    private static void addOwner(EntityDao entityDao, String[] slowa) {

        String name = slowa[2]; // alias dla name, stworzyłem zmienną żeby nie używać bardzo nieczytelnego słowa[2]

        Owner owner = new Owner(name, slowa[3], slowa[4]); // imie, nazwisko, PESEL
        entityDao.save(owner);

    }
}
